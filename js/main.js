$(document).ready(function() {

  $('.datepicker1').datepicker({
    autoclose: true
  });

  $('.dateyear').datepicker({
    autoclose: true,
    minViewMode: 2,
    format: 'yyyy'
  });


  if ($(window).width() < 992) {

    $('.toggle_side_btn').on('click', function() {
      $('.sidebar').addClass('op_sidebar')
    })

    $('body').click(function(e) {
      if (!$(e.target).is('.toggle_side_btn, .toggle_side_btn *, .sidebar, .sidebar *')) {
        $('.sidebar').removeClass('op_sidebar')
      }
    })

  }
  if ($(window).width() < 767) {
      $('.sidebar button.exist').after($('.general_top ul'))

  }
/*******************/
$('.accord_top_div').on('click', function() {
  // e.preventDefault()

    if($(this).parent().parent().hasClass("thi")){

      $(this).parent().parent().siblings().find('>li').removeClass('active_accord')
      $(this).parent().parent().siblings().find('.accord_top').removeClass('op')
      $(this).parent().parent().siblings().find('.accord_top').next().slideUp()

      $(this).parent().toggleClass('active_accord')
      $(this).toggleClass('op')
      $(this).next().slideToggle()

    }
  })
/*******************/

  $('a.accord_top').on('click', function(e) {
    e.preventDefault()

    if($(this).parent().parent().hasClass("thi")){

      $(this).parent().parent().siblings().find('>li').removeClass('active_accord')
      $(this).parent().parent().siblings().find('.accord_top').removeClass('op')
      $(this).parent().parent().siblings().find('.accord_top').next().slideUp()

      $(this).parent().toggleClass('active_accord')
      $(this).toggleClass('op')
      $(this).next().slideToggle()

    }
    else{
      $(this).toggleClass('op')
      $(this).parent().toggleClass('active_accord')
      $(this).next().slideToggle()
    }
  })

  $('a.full_toggle').on('click', function(e) {
    e.preventDefault();
    $(this).parent().parent().parent().toggleClass('full_block')
  })

  $('a.collapse_toggle').on('click', function(e) {
    e.preventDefault();
    $(this).parent().parent().next().slideToggle()
  })

  $('a.delete_panel').on('click', function(e) {
    e.preventDefault();
    $(this).parent().parent().parent().remove()
  })

})